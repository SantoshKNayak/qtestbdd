Feature:
 Test Inventory

Scenario: Items returned for refund
Given That a customer previously bought a black sweater from me
And I have three black sweater in inventory
When they return the black sweater for a refund
Then I should have four black sweater in inventory